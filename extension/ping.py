from discord.ext import commands


@commands.command()
async def ping(ctx: commands.Context):
    latency = ctx.bot.latency * 1000
    await ctx.send("Pong !\nLatence : {:.2f} ms".format(latency))


def setup(bot: commands.Bot):
    bot.add_command(ping)
