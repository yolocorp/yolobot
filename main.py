import logging

import discord
from discord.ext import commands

import config
from utility.help import YoloBotHelp

logging.basicConfig(level=logging.INFO)

# Création du bot
intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(
    config.prefix,
    help_command=YoloBotHelp(
        commands_heading="Commandes :",
        aliases_heading="Alias :",
        no_category="Autre",
        command_attrs={'aliases': ["aide"], 'help': "Affiche ce message."}
    ),
    intents=intents
)


# Affiche un message quand le bot est prêt à être utilisé.
@bot.event
async def on_ready():
    print("Le bot est prêt à dominer le monde : {0.user}".format(bot))


# Chargement des extensions
extensions = (
    'ping',
)
for ext in extensions:
    bot.load_extension('extension.' + ext)

# Chargement des cogs
cogs = (
    'spam',
    'level.level',
    'level.message_exp',
    'level.voice_exp',
    'admin',
    'survey.survey',
    'votekick.votekick'
)

for cog in cogs:
    bot.load_extension('cog.' + cog)

# Lancement du bot
bot.run(config.discord_token)
