def keycap_number(n: int):
    assert 0 <= n <= 10

    if n == 10:
        return "\U0001f51f"

    return "{:c}\ufe0f\u20e3".format(48 + n)
