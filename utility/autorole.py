import discord

import config


async def update_role(user: discord.Member, level, channel: discord.TextChannel = None):
    # On ne fait rien si le membre possède l'un des rôles ignorés.
    if discord.utils.find(lambda r: r.id in config.ignore_auto_roles, user.roles) is not None:
        return

    # On récupère l'auto-role (rôle qui correspond à un palier) actuel du membre.
    current_role = discord.utils.find(lambda r: r.id in config.auto_roles.values(), user.roles)

    # On cherche le palier le plus élevé (key) sans qu'il ne dépasse le niveau (level).
    # Exemple avec les paliers 2 et 10 :
    # level = 5 : key = 1
    # level = 11 : key = 10
    # level = 1 : aucun palier ne correspond
    for k in sorted(config.auto_roles.keys(), reverse=True):
        if level >= k:
            key = k
            break
    else:
        # Si aucun palier ne correspond, on effectue seulement la suppression de l'auto-role du membre.
        if current_role is not None:
            await user.remove_roles(current_role)
        return

    # On récupère le rôle associé au palier.
    role = user.guild.get_role(config.auto_roles[key])

    # On ne fait rien si le membre possède déjà l'auto-role.
    if current_role == role:
        return

    # On lui ajoute le nouvel auto-role.
    await user.add_roles(role)

    # Et on lui supprime l'ancien.
    if current_role is not None:
        await user.remove_roles(current_role)

    if channel is not None:
        await channel.send("🎉 Nouveau rôle pour {} : **{}**".format(user.mention, role.name))
