from discord.ext import commands


class YoloBotHelp(commands.MinimalHelpCommand):
    def get_opening_note(self):
        command_name = self.invoked_with
        return "Utilisez `{0}{1} commande` pour obtenir plus d'infos sur une commande.\n" \
               "Vous pouvez aussi utiliser `{0}{1} catégorie` pour obtenir plus d'infos sur une catégorie." \
            .format(self.clean_prefix, command_name)

    def command_not_found(self, string: str):
        return 'La commande `{}` est inconnue.'.format(string)

    def subcommand_not_found(self, command: commands.Command, string: str):
        if isinstance(command, commands.Group) and len(command.all_commands) > 0:
            return "La commande `{0.qualified_name}` n'a aucune sous-commande nommée `{1}`.".format(command, string)
        return "La commande `{0.qualified_name}` n'a aucune sous-commande.".format(command)
