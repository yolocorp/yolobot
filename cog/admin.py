from discord.ext import commands


class Admin(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.group(aliases=["ext"], invoke_without_command=True)
    @commands.has_permissions(administrator=True)
    async def extension(self, ctx: commands.Context):
        await ctx.send_help(self.extension)

    @extension.command("reload", aliases=["r"])
    async def extension_reload(self, ctx: commands.Context, ext_name: str):
        self.bot.reload_extension(ext_name)
        await ctx.send("👌🏽")


def setup(bot):
    bot.add_cog(Admin(bot))
