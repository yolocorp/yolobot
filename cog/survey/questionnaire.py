import asyncio
import io
from typing import Dict

import discord
from matplotlib import pyplot as plt

from cog.survey.answer import Answer


class Questionnaire:
    def __init__(self, statement: str, author: discord.User = None):
        self.statement = statement
        self.author = author
        self._answers: Dict[str, Answer] = dict()

    def add_or_edit_answer(self, emoji: str, statement: str):
        self._answers[emoji] = Answer(emoji, statement)

    def remove_answer(self, emoji):
        del self._answers[emoji]

    def increment_answer(self, emoji: str):
        self._answers[emoji].count += 1

    def decrement_answer(self, emoji: str):
        self._answers[emoji].count -= 1

    def emojis(self):
        return self._answers.keys()

    def message_content(self):
        content = "**" + self.statement + "**\n\n"

        for e, s in self._answers.items():
            content += e + " " + s.statement + "\n"

        content += "\nProposé par {}".format(self.author.mention)

        return content

    def results(self):
        answers = self._answers.values()
        max_count = max(answers, key=lambda answer: answer.count).count

        content = "**" + self.statement + "**\nVoici les résultats :\n\n"

        for a in answers:
            if a.count > 0 and a.count == max_count:
                content += "**" + a.statement + " : " + str(a.count) + "**\n"
            else:
                content += a.statement + " : " + str(a.count) + "\n"

        content += "\nProposé par {}".format(self.author.mention)

        if max_count == 0:
            return content, None

        counts = list()
        explode = list()
        labels = list()

        for a in self._answers.values():
            if a.count > 0:
                labels.append(a.statement)
                counts.append(a.count)
                if a.count == max_count:
                    explode.append(0.1)
                else:
                    explode.append(0)

        fig: plt.Figure
        ax: plt.Axes
        fig, ax = plt.subplots()
        ax.pie(counts, explode, labels, autopct="%1.1f%%", shadow=True)
        ax.axis("equal")

        file = io.BytesIO()
        file.name = "survey.png"
        fig.savefig(file, format="png", bbox_inches="tight")
        file.seek(0)

        return content, discord.File(file)

    async def send_results(self, time: int, channel: discord.TextChannel):
        await asyncio.sleep(time)
        content, file = self.results()
        await channel.send(content, file=file)
