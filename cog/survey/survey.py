import re
from typing import Dict

import discord
from discord.ext import commands

from cog.survey.questionnaire import Questionnaire
from utility.utils import keycap_number


class Survey(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.questionnaires: Dict[int, Questionnaire] = dict()

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction: discord.Reaction, user: discord.User):
        if reaction.message.id not in self.questionnaires or user == self.bot.user:
            return

        self.questionnaires[reaction.message.id].increment_answer(str(reaction))

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction: discord.Reaction, user: discord.User):
        if reaction.message.id not in self.questionnaires or user == self.bot.user:
            return

        self.questionnaires[reaction.message.id].decrement_answer(str(reaction))

    @commands.group(aliases=["poll", "questionnaire"], invoke_without_command=True)
    async def survey(self, ctx: commands.Context):
        await ctx.send_help(self.survey)

    @survey.command("create",
                    help="Créer un sondage avec entre 2 et 10 choix possibles.\n"
                         "Les résultats seront envoyés au bout de `time` minutes/heures.\n"
                         "\n"
                         "Exemples :\n"
                         "`survey create 15 Pain au chocolat ou chocolatine ? | Pain au chocolat | Chocolatine` : "
                         "2 choix et 15 minutes pour répondre.\n"
                         "`survey create 2h Quel est ton genre ? | Féminin | Masculin | Non-binaire` : "
                         "3 choix et 2 heures pour répondre.",
                    usage="<time[h]> question | choice 1 | choice 2 [ | choice 3 | ... | choice 10]")
    async def survey_create(self, ctx: commands.Context, time: str, *, args: str):
        args = re.split(r"\s*\|\s*", args)
        answers = args[1:]

        if len(answers) > 10 or len(answers) < 2:
            return

        if time.endswith(("h", "H")):
            time = int(time[:-1]) * 3600
        else:
            time = int(time) * 60

        questionnaire = Questionnaire(discord.utils.escape_markdown(args[0]), ctx.author)

        for i, s in enumerate(answers):
            questionnaire.add_or_edit_answer(keycap_number(i + 1), discord.utils.escape_markdown(s))

        message = await ctx.send(questionnaire.message_content())

        for e in questionnaire.emojis():
            await message.add_reaction(e)

        self.questionnaires[message.id] = questionnaire

        await questionnaire.send_results(time, ctx.channel)


def setup(bot):
    bot.add_cog(Survey(bot))
