from __future__ import annotations

import asyncio
from typing import Dict

import discord


class VoteMessage:
    messages: Dict[int, VoteMessage] = dict()
    emoji = "\u2705"

    def __init__(self, member: discord.Member, reason: str, required_votes: int, time_limit: int):
        self.member = member
        self.reason = reason
        self.required_votes = required_votes
        self.time_limit = time_limit
        self._votes = 0
        self._message = None
        self._task = None

    async def add_vote(self):
        self._votes += 1

        if self._votes >= self.required_votes:
            await self.end()

    def remove_vote(self):
        self._votes -= 1

    async def send(self, channel: discord.TextChannel):
        self._message = await channel.send(
            "**Demande d'expulsion pour {}.**\n"
            "Raison : {}\n"
            "Nombre de votes requis : {}\n"
            "Vous avez {} minutes pour voter."
                .format(self.member.mention, self.reason, self.required_votes, self.time_limit)
        )

        async def wait(time):
            await asyncio.sleep(time * 60)
            await self.end()

        self._task = asyncio.create_task(wait(self.time_limit))

        await self._message.add_reaction(self.emoji)
        self.messages[self._message.id] = self

    async def end(self):
        del self.messages[self._message.id]

        if self._votes >= self.required_votes:
            self._task.cancel()
            await self.member.kick(reason="[votekick] " + self.reason)
            await self._message.channel.send(
                "{} a été expulsé du serveur suite à un vote.\n"
                "Raison : {}"
                    .format(self.member.mention, self.reason)
            )
        else:
            await self._message.channel.send(
                "{} a réussi à éviter l'expulsion, bien joué à lui.\n"
                "Votes : {}/{}"
                    .format(self.member.mention, self._votes, self.required_votes)
            )
