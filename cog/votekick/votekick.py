import discord
from discord.ext import commands

from cog.votekick.vote_message import VoteMessage


class Votekick(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_reaction_add(self, reaction: discord.Reaction, user: discord.User):
        message_id = reaction.message.id

        if message_id not in VoteMessage.messages or user == self.bot.user:
            return

        if reaction.emoji == VoteMessage.emoji:
            await VoteMessage.messages[message_id].add_vote()

    @commands.Cog.listener()
    async def on_reaction_remove(self, reaction: discord.Reaction, user: discord.User):
        message_id = reaction.message.id

        if message_id not in VoteMessage.messages or user == self.bot.user:
            return

        if reaction.emoji == VoteMessage.emoji:
            VoteMessage.messages[message_id].remove_vote()

    @commands.Command
    @commands.guild_only()
    @commands.has_permissions(kick_members=True)
    async def votekick(self, ctx: commands.Context, member: discord.Member, reason: str, required_votes: int,
                       time_limit: int):
        await VoteMessage(member, reason, required_votes, time_limit).send(ctx.channel)


def setup(bot):
    bot.add_cog(Votekick(bot))
