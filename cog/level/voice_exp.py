from discord.ext import commands, tasks

import config


class VoiceExp(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.level = bot.get_cog("Level")
        self.exp_add_task.start()

    @tasks.loop(minutes=5)
    async def exp_add_task(self):
        for guild in self.bot.guilds:
            for channel in guild.voice_channels:
                if len(channel.members) <= 1 or channel == guild.afk_channel:
                    continue

                for member_id, voice in channel.voice_states.items():
                    member = guild.get_member(member_id)
                    exp = config.voice_exp

                    if voice.self_stream:
                        exp *= 1.5

                    if voice.self_mute or voice.mute:
                        exp /= 2

                    if voice.self_deaf or voice.deaf:
                        exp /= 2

                    await self.level.api.exp_patch(member, "add", round(exp))

    def cog_unload(self):
        self.exp_add_task.cancel()


def setup(bot):
    bot.add_cog(VoiceExp(bot))
