import io

import discord
import imgkit
from discord.ext import commands

from cog.level.api import LevelApi
from utility.utils import keycap_number


class Level(commands.Cog):
    def __init__(self, bot):
        self.api = LevelApi(bot)

    @commands.group(help="Alias pour `level show`.", invoke_without_command=True)
    @commands.guild_only()
    async def level(self, ctx: commands.Context, user: discord.Member = None):
        await ctx.invoke(self.level_show, user)

    @level.command("show", help="Affiche le niveau et les points d'expérience de vous-même ou d'un autre membre.")
    async def level_show(self, ctx: commands.Context, user: discord.Member = None):
        user = user if user is not None else ctx.author
        if user.bot:
            return

        level = self.api.read(user)

        img = imgkit.from_url(self.api.card_url(user), False, {
            "width": "500",
            "quiet": "",
            "custom-header": [('X-AUTH-TOKEN', self.api.session.headers['X-AUTH-TOKEN'])]
        })
        file = io.BytesIO(img)
        file.name = "user_card.png"

        async with ctx.typing():
            await ctx.send(
                "**{}**\n"
                "Niveau : **{}**\n"
                "Points d'expérience : **{}**/{}"
                    .format(user.display_name, level["level"], level["exp"], level["exp_next"]),
                file=discord.File(file)
            )

    @level.command("add", help="Ajoute des points d'expérience à un membre.")
    @commands.has_permissions(administrator=True)
    async def level_add(self, ctx: commands.Context, amount: int, user: discord.Member, notify: bool = False):
        await self.api.exp_patch(user, "add", amount, notify)

    @level.command("remove", help="Retire des points d'expérience à un membre.")
    @commands.has_permissions(administrator=True)
    async def level_remove(self, ctx: commands.Context, amount: int, user: discord.Member, notify: bool = False):
        await self.api.exp_patch(user, "remove", amount, notify)

    @level.command("ranking", aliases=["rank"], help="Affiche le classement du serveur.")
    async def level_ranking(self, ctx: commands.Context):
        guild = ctx.guild
        levels = self.api.ranking(guild)
        content = "Classement sur le serveur {} :\n".format(guild.name)

        rank = 1
        for level in levels:
            user = guild.get_member(level["discord_id"])
            content += "\n{} **{}** : niv. {} (exp. {})".format(
                keycap_number(rank),
                user.display_name,
                level["level"],
                level["exp"]
            )
            rank += 1

        await ctx.send(content)


def setup(bot):
    bot.add_cog(Level(bot))
