import discord
import requests

import config
from utility.autorole import update_role


class LevelApi:
    def __init__(self, bot):
        self.base_url = config.yoloweb_base_url + "/level"
        self.auto_role = bot.get_cog("AutoRole")
        self.session = requests.Session()
        self.session.headers = {'X-AUTH-TOKEN': config.yoloweb_token}

    def create(self, member: discord.Member):
        r = self.session.post(
            self.base_url + "/{}/{}".format(
                member.guild.id,
                member.id
            ),
            timeout=60
        )

        r.raise_for_status()

        return r.json()

    def read(self, member: discord.Member):
        r = self.session.get(
            self.base_url + "/{}/{}".format(
                member.guild.id,
                member.id
            ),
            timeout=60
        )

        r.raise_for_status()

        return r.json()

    def update(self, member: discord.Member, data):
        r = self.session.put(
            self.base_url + "/{}/{}".format(
                member.guild.id,
                member.id
            ),
            timeout=60,
            json=data
        )

        r.raise_for_status()

        return r.json()

    def ranking(self, guild: discord.Guild):
        r = self.session.get(self.base_url + "/{}/ranking".format(guild.id), timeout=60)

        r.raise_for_status()

        return r.json()

    async def exp_patch(self, member: discord.Member, operation, amount,
                        notify=False, channel: discord.TextChannel = None):
        r = self.session.patch(
            self.base_url + "/{}/{}/exp".format(
                member.guild.id,
                member.id
            ),
            timeout=60,
            data={
                "operation": operation,
                "amount": amount
            }
        )

        r.raise_for_status()

        channel = channel if channel is not None else member.guild.get_channel(config.new_level_channel)

        data = r.json()
        if data["new_level"]:
            level = data["0"]["level"]

            await update_role(member, level, channel)

            if notify:
                await channel.send("🎊 {} est maintenant niveau **{}**".format(member.mention, level))

        return data

    def card_url(self, member: discord.Member):
        return self.base_url + "/{}/{}/card".format(
            member.guild.id,
            member.id
        )
