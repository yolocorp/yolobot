import discord
from discord.ext import commands

import config


class MessageExp(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.level = bot.get_cog("Level")
        self.spam = bot.get_cog("Spam")

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        author = message.author
        if (author == self.bot.user
                or message.is_system() or message.guild is None
                or message.content.startswith(self.bot.command_prefix)):
            return

        exp = config.message_exp * len(message.content) / 10
        exp += config.message_exp * 2 * len(message.attachments)

        if not self.spam.is_spamming(author):
            await self.level.api.exp_patch(author, "add", round(exp), True, message.channel)
        else:
            await self.level.api.exp_patch(author, "remove", config.message_exp * 2)


def setup(bot):
    bot.add_cog(MessageExp(bot))
