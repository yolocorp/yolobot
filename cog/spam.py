from datetime import timedelta, datetime
from typing import Dict, Tuple

import discord
from discord.ext import commands

import config


class Spam(commands.Cog):
    def __init__(self, bot: commands.Bot):
        self.bot = bot
        self.members: Dict[discord.Member, Tuple[datetime, int]] = dict()

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        author = message.author
        if author.bot \
                or author == self.bot.user \
                or message.guild is None:
            return

        created_at = message.created_at

        if author in self.members:
            date, nb = self.members[author]

            if created_at - date < timedelta(seconds=config.time_limit):
                # On incrémente le compteur de message
                # si l'auteur a déjà envoyé un autre message dans les time_limit dernières secondes.
                nb += 1
                if nb > config.msg_limit:
                    # Actions à réaliser lorsque qu'un spam est détecté.
                    # On ne réinitialise pas son compteur tant que l'auteur du message
                    # n'a pas attendu time_limit secondes depuis la dernière détection de spam.
                    date = created_at
            else:
                # On réinitialise le compteur de message de l'auteur.
                date, nb = created_at, 1
        else:
            # On ajoute l'auteur du message dans le dictionnaire si ça n'a pas déjà été fait,
            # on enregistre la date du message et on initialise le compteur de message à 1.
            date, nb = created_at, 1

        self.members[author] = date, nb

    @commands.command()
    @commands.has_permissions(administrator=True)
    async def debug_spam(self, ctx: commands.Context):
        await ctx.send(str(self.members))

    def is_spamming(self, member: discord.Member):
        if member not in self.members:
            return False

        return self.members[member][1] > config.msg_limit


def setup(bot):
    bot.add_cog(Spam(bot))
